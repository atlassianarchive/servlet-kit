The Servlet Kit provides a helper library for writing a remotable Atlassian Plugin.

Features:

- Convention-based class scanning for components and servlets
- Basic templating support remote pages
- OSGi manifest generation assistence via a hard dependency on common packages
- Multipage support that maintains a connect to the parent page

To use this kit, add this to your Maven project:

    <dependency>
      <groupId>com.atlassian.pluginkit</groupId>
      <artifactId>servlet-kit</artifactId>
      <version>LATEST</version>
    </dependency>
