package com.atlassian.pluginkit.servlet.spring.ns;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * Namespace handler for all remote plugin spring elements
 */
public class ServletKitNamespaceHandler extends NamespaceHandlerSupport {

    public void init() {
        registerBeanDefinitionParser("bean-scan", new BeanScanBeanDefinitionParser());
    }
}
